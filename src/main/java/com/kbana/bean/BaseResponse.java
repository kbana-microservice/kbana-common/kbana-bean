package com.kbana.bean;

import com.kbana.constant.ResponseCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BaseResponse implements Serializable {
    private String code;
    private String message;
    private Object data;

    public static BaseResponse of(Object data) {
        return of(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getMessage(), data);
    }

    public static BaseResponse of(ResponseCode responseCode) {
        return of(responseCode.getCode(), responseCode.getMessage());
    }

    public static BaseResponse of(ResponseCode responseCode, Object data) {
        return of(responseCode.getCode(), responseCode.getMessage(), data);
    }

    public static BaseResponse of(String code, String message) {
        return of(code, message, null);
    }

    public static BaseResponse of(String code, String message, Object data) {
        return BaseResponse.builder().code(code).message(message).data(data).build();
    }
}
