package com.kbana.bean.dto.user;

import com.kbana.bean.dto.group.CreateUserValid;
import com.kbana.bean.dto.group.UpdateUserValid;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDTO implements Serializable {

    @NotBlank(groups = UpdateUserValid.class)
    private Long id;

    @NotBlank(groups = CreateUserValid.class)
    private String username;

    @NotBlank(groups = CreateUserValid.class)
    @Pattern(groups = CreateUserValid.class, regexp = "^(?=.*[0-9])(?=.*[A-Za-z])(?=\\S+$).{6,}$", message =
            "password must contain at least one character, one digit number, do not have space and length >= 6")
    private String password;

    @NotBlank(groups = CreateUserValid.class)
    @NotBlank(groups = UpdateUserValid.class)
    private String fullname;

    @NotBlank(groups = CreateUserValid.class)
    @NotBlank(groups = UpdateUserValid.class)
    private String email;

    @NotBlank(groups = CreateUserValid.class)
    @NotBlank(groups = UpdateUserValid.class)
    private String phone;

    private Timestamp createDate;

    private Timestamp updateDate;

    private Long status;

}
