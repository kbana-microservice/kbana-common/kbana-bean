package com.kbana.constant;

public interface IResponseCode {
    String getCode();
    String getMessage();
}
