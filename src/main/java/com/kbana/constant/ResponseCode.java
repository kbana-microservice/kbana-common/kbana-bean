package com.kbana.constant;

public enum ResponseCode implements IResponseCode {

    SUCCESS("00", "Success"),
    INVALID_REQUEST("01", "Invalid request"),
    TIMEOUT("08", "Doubts"),
    UNAUTHORIZED("10", "Unauthorized"),
    FORBIDDEN("11", "Forbidden"),
    USER_NOT_FOUND("81", "user is not found"),
    USE_NAME_ALREADY_USED("85", "username is already used"),
    WRONG_PASSWORD("86", "wrong password"),
    PHONE_ALREADY_USED("87", "phone is already used"),
    EXCEPTION("99", "Internal System Error...!");


    ResponseCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
