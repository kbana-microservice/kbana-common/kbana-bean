package com.kbana.exception;

import com.kbana.constant.IResponseCode;
import lombok.Getter;

@Getter
public class KbanaException extends Exception {
    private final String code;

    public KbanaException(String code) {
        this.code = code;
    }

    public KbanaException(String code, String message) {
        super(message);
        this.code = code;
    }

    public KbanaException(String code, String message, Throwable throwable) {
        super(message, throwable);
        this.code = code;
    }

    public KbanaException(String code, Throwable throwable) {
        super(throwable);
        this.code = code;
    }

    public KbanaException(IResponseCode iResponseCode) {
        super(iResponseCode.getMessage());
        this.code = iResponseCode.getCode();
    }
}
